import time
import logging
import re
from collections.abc import Iterator

import grequests

from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import List, Dict, Union
from itertools import batched
from bs4 import BeautifulSoup

from util import grequest_exception_handler, convert_string_to_grams

LOGGER = logging.getLogger()

URLS = [
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=09&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=18&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=07&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=15&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=14&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=19&naFlag=1",
    "https://foodpro.students.vt.edu/menus/MenuAtLocation.aspx?locationNum=16&naFlag=1",
]


@dataclass
class DatedPage:
    url: str
    date: datetime.date


@dataclass
class FoodPreFetch:
    food_name: str
    date: datetime.date
    serving_time: str
    location: str
    food_facts_url: str


@dataclass
class Nutrients:
    """
    NaN when fails to scrape floats
    None when fails to scrape allergens
    """
    allergens: Union[None, List[str]]
    calories: float
    fat: float
    cholesterol: float
    sodium: float
    carbohydrates: float
    sugar: float
    protein: float


@dataclass
class Food:
    food_name: str
    nutrients: Nutrients
    date: datetime.date
    serving_time: str
    location: str


class FoodScraperIterator(Iterator):
    def __init__(self, max_threads: int = 2,
                 batch_size: int = 10,
                 idle_time: float = 4):

        self._base_urls: List[str] = URLS
        self._batch_size = batch_size
        self._max_threads = max_threads
        self._idle_time = idle_time

        self._fetched_foods_cache: Dict[str, Nutrients] = {}
        self._next_foods: List[Food] = []
        self._last_fetched: datetime = datetime.now()
        self._prefetched_foods: List[FoodPreFetch] = []

        dated_pages = self._get_dated_pages()
        self._prefetch_foods(dated_pages)

    def __next__(self) -> Food:
        if len(self._prefetched_foods) == 0:
            raise StopIteration()

        if len(self._next_foods) > 0:
            return self._next_foods.pop()

        num_foods_fetch = min(len(self._prefetched_foods), self._batch_size)
        foods_to_fetch = [
            self._prefetched_foods.pop() for _ in
            range(num_foods_fetch)
        ]

        self._fetch_food_data(foods_to_fetch)

        return self._next_foods.pop()

    def _throtle_requests(self) -> None:
        if self._last_fetched > datetime.now() + timedelta(seconds=self._idle_time):
            time.sleep(self._idle_time)

        self._last_fetched = datetime.now()

    def _get_dated_pages(self) -> List[DatedPage]:
        rs = (grequests.get(url) for url in self._base_urls)
        responses = grequests.imap(rs,
                                   size=self._max_threads,
                                   exception_handler=grequest_exception_handler,
                                   )

        dated_pages: List[DatedPage] = []
        for response in responses:
            if not response.ok:
                LOGGER.warning(f"The response for {response.url} is "
                               f"{response.status_code}.\n"
                               f"Continue scraping next URL...")
                continue

            data = BeautifulSoup(response.text, "html.parser")
            available_dates_data = (
                data.find_all("select",
                              class_="form-control show_loading").pop().findChildren()
            )
            for date_data in available_dates_data:
                date_value = datetime.strptime(date_data['value'], "%m/%d/%Y").date()
                if date_value < datetime.now().date():
                    continue

                target_url = date_data['data-target-url']
                date_url = (
                    f"https://foodpro.students.vt.edu/menus/{target_url}"
                )
                dated_pages.append(DatedPage(date_url, date_value))

        self._throtle_requests()

        return dated_pages

    def _prefetch_foods(self, dated_pages: List[DatedPage]) -> None:
        for pages in batched(dated_pages, self._batch_size):
            reqs = [grequests.get(page.url) for page in pages]
            resps = (grequests.
                     imap_enumerated(reqs,
                                     size=self._max_threads,
                                     exception_handler=grequest_exception_handler
                                     ))
            for idx, resp in resps:
                date = dated_pages[idx].date
                if not resp.ok:
                    LOGGER.warning(f"The response for {resp.url} is "
                                   f"{resp.status_code}.\n"
                                   f"Continue scraping next URL...")
                    continue

                data = BeautifulSoup(resp.text, "html.parser")
                hall_name = data.find_all("h2")[0].text
                meal_times = data.find_all("a", class_="nav-link")
                meal_times = [meal.text.strip() for meal in meal_times]
                meal_menus = data.find_all("div", class_=(
                    lambda s: s is not None and "tab-pane fade" in s),
                                           )
                for menu, meal_time in zip(meal_menus, meal_times):
                    for div in menu.find_all("div", class_="recipe_title"):
                        a = div.findChildren().pop()
                        destination_shorthand_url = a["href"]
                        if "label.aspx?" not in destination_shorthand_url:
                            LOGGER.info(
                                f"Found url that does not match a dinning hall format"
                                f" for {destination_shorthand_url}.\n "
                                f"Continue scraping next URL...")
                            continue

                        pattern = r'RecNumAndPort=[^&]*'
                        match = re.search(pattern, destination_shorthand_url)
                        match = match.group()
                        food_data_url = (f"https://foodpro.students.vt.edu/menus/"
                                         f"label.aspx?{match}")
                        food_name = a.text.strip()
                        if "breakfast" in meal_time.lower() or "daily items" in meal_time.lower():
                            prefetched_food = FoodPreFetch(food_name=food_name,
                                                           date=date,
                                                           serving_time="Breakfast",
                                                           location=hall_name,
                                                           food_facts_url=food_data_url,
                                                           )
                            self._prefetched_foods.append(prefetched_food)
                        if "lunch" in meal_time.lower() or "daily items" in meal_time.lower():
                            prefetched_food = FoodPreFetch(food_name=food_name,
                                                           date=date,
                                                           serving_time="Lunch",
                                                           location=hall_name,
                                                           food_facts_url=food_data_url,
                                                           )
                            self._prefetched_foods.append(prefetched_food)
                        if "dinner" in meal_time.lower() or "daily items" in meal_time.lower():
                            prefetched_food = FoodPreFetch(food_name=food_name,
                                                           date=date,
                                                           serving_time="Dinner",
                                                           location=hall_name,
                                                           food_facts_url=food_data_url,
                                                           )
                            self._prefetched_foods.append(prefetched_food)

                        self._throtle_requests()

    def _fetch_food_data(self, prefetched_foods: List[FoodPreFetch]) -> None:
        needs_fetching: List[FoodPreFetch] = []

        for prefetched_food in prefetched_foods:
            if prefetched_food.food_facts_url in self._fetched_foods_cache:
                food_name: str = prefetched_food.food_name
                nutrients: Nutrients = self._fetched_foods_cache[
                    prefetched_food.food_facts_url]
                date: datetime.date = prefetched_food.date
                serving_time: str = prefetched_food.serving_time
                location: str = prefetched_food.location

                new_food = Food(food_name, nutrients, date, serving_time, location)
                self._next_foods.append(new_food)
            else:
                needs_fetching.append(prefetched_food)

        reqs = (grequests.get(prefetched_food.food_facts_url) for prefetched_food in
                needs_fetching)
        resps = grequests.imap_enumerated(reqs, size=self._max_threads,
                                          exception_handler=grequest_exception_handler)

        for idx, resp in resps:
            prefetched_food = needs_fetching[idx]
            if not resp.ok:
                LOGGER.warning(f"The response for {resp.url} is "
                               f"{resp.status_code}.\n"
                               f"Continue scraping next URL..."
                               f"Happened when fetching food data.")
                continue

            data = BeautifulSoup(resp.text, "html.parser")

            # Allergens scraping.
            try:
                allergens_str: str = (
                    data.find_all("div", class_="allergens_container")[0].text)
                allergens_str = allergens_str.split(":")[1]
                allergens: Union[None, List[str]] = allergens_str.split(",")
                allergens = [
                    allergen.strip() for allergen in allergens
                    if len(allergen.strip()) > 0
                ]
            except Exception as e:
                LOGGER.error(
                    f"Failed to scrape allergens from "
                    f"{prefetched_food.food_facts_url}.\n"
                    f"The error message is {e}."
                )
                allergens = None
            # Calorie scraping.
            try:
                calories_str = (data
                                .find("div",
                                      {"id": "calories_container"})
                                .text.strip()
                                )
                calories: float = float(calories_str.split()[1])
            except Exception as e:
                LOGGER.error(f"Failed to scrape calories from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                calories = float("NaN")

            # Fat scraping.
            try:
                fat: float = FoodScraperIterator._scrape_nutrient_value(data,
                                                                        "total_fat",
                                                                        "Total Fat")
            except Exception as e:
                LOGGER.error(f"Failed to scrape fat from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                fat = float("NaN")
            # Cholesterol scraping.
            try:
                cholesterol: float = (FoodScraperIterator.
                                      _scrape_nutrient_value(data,
                                                             "cholesterol",
                                                             "Cholesterol")
                                      )
            except Exception as e:
                LOGGER.error(f"Failed to scrape cholesterol from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                cholesterol = float("NaN")

            # Sodium scraping.
            try:
                sodium: float = (FoodScraperIterator.
                                 _scrape_nutrient_value(data, "sodium",
                                                        "Sodium")
                                 )
            except Exception as e:
                LOGGER.error(f"Failed to scrape sodium from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                sodium = float("NaN")

            # Carbohydrates scraping.
            try:
                carbohydrates: float = (FoodScraperIterator.
                                        _scrape_nutrient_value(data,
                                                               "total_carbohydrates",
                                                               "Tot. Carb."))
            except Exception as e:
                LOGGER.error(f"Failed to scrape carbs from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                carbohydrates = float("NaN")

            # Sugar scraping.
            try:
                sugar: float = (FoodScraperIterator.
                                _scrape_nutrient_value(data, "sugars",
                                                       "Sugars",
                                                       "col-lg-12")
                                )
            except Exception as e:
                LOGGER.error(f"Failed to scrape sugar from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                sugar = float("NaN")

            # Protein scraping.
            try:
                protein: float = (
                    FoodScraperIterator.
                    _scrape_nutrient_value(data, "protein",
                                           "Protein",
                                           "col-lg-12")
                )
            except Exception as e:
                LOGGER.error(f"Failed to scrape protein from "
                             f"{prefetched_food.food_facts_url}.\n"
                             f"The error message is {e}."
                             )
                protein = float("NaN")

            new_nutrients: Nutrients = Nutrients(allergens,
                                                 calories,
                                                 fat,
                                                 cholesterol,
                                                 sodium,
                                                 carbohydrates,
                                                 sugar,
                                                 protein,
                                                 )
            self._fetched_foods_cache[prefetched_food.food_facts_url] = new_nutrients

            food_name = prefetched_food.food_name
            date = prefetched_food.date
            serving_time = prefetched_food.serving_time
            location = prefetched_food.location

            new_food = Food(food_name, new_nutrients, date, serving_time, location)
            self._next_foods.append(new_food)

        self._throtle_requests()

    @staticmethod
    def _scrape_nutrient_value(data: BeautifulSoup,
                               div_name: str,
                               nutrient_name: str,
                               column_location: str =
                               "col-lg-8 col-md-8 col-sm-8 col-xs-8") -> float:
        nutrient_div = data.find("div", class_=div_name)
        nutrient_val_string: str = \
            (nutrient_div.
             find("div",
                  class_=column_location
                  ).
             text.strip()
             )
        nutrient_val_string = nutrient_val_string.replace(nutrient_name, "")
        return convert_string_to_grams(nutrient_val_string)


def main():
    scraper = FoodScraperIterator()
    for food in scraper:
        print(food)


if __name__ == '__main__':
    main()
