FROM python:3.12.2-alpine3.18

WORKDIR /usr/src/app 

COPY . .

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install mysql-connector-python

EXPOSE 80

CMD ["python", "insert-data-scraper.py"]