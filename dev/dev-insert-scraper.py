import sys
import path
# directory reach
directory = path.Path(__file__).abspath()
 
# setting path
sys.path.append(directory.parent.parent)

import scraper
import mysql.connector
from mysql.connector import Error
import os
from datetime import datetime, timedelta
'''
CODE FOR DEVELOPMENT ENVIRONMENT. NOT USED ON PRODUCTION.
'''
def delete_data(db_connection, cursor, n=2):
    '''
    Delete data that is n days old.
    '''
    # n_days_ago = datetime.now() - timedelta(days=n)
    # formatted_date = n_days_ago.strftime('%Y-%m-%d')

    delete_food_date_query = """DELETE FROM FoodDate WHERE DATEDIFF(NOW(), foodDateDate) = %s"""
    delete_food_query = """DELETE FROM Food WHERE NOT EXISTS ( SELECT * FROM FoodDate WHERE FoodDate.foodID = Food.foodID );"""

    try:
        cursor.execute(delete_food_date_query, (n,))
        cursor.execute(delete_food_query)
        db_connection.commit()
    except Error as e:
        print(e)
        db_connection.rollback()
    
    print(f"Deleted all Food data that is {n} days old from the FoodDate table and no reoccurence in the FoodDate table.")

def hard_delete_data(db_connection, cursor):
    '''
    Delete old data from the Meal, FoodDate, and Food table before inserting data.
    '''

    delete_stmt_meal = """DELETE FROM Meal WHERE mealID >= 1"""
    delete_stmt_food = """DELETE FROM Food WHERE foodID >= 1"""
    delete_stmt_fooddate = """DELETE FROM FoodDate WHERE foodDateID >= 1"""

    try:
        cursor.execute(delete_stmt_meal)
        cursor.execute(delete_stmt_fooddate)
        cursor.execute(delete_stmt_food)
        db_connection.commit()

    except Error as e:
        print(e)
        db_connection.rollback()
    
    print("Deleted ALL data from Meal, Food, and FoodDate tables")

def insert_into_food_table(db_connection, cursor, fooditerator):
    '''
    Given a database connection and scraper iterator, insert into the database.
    '''
    print("Inserting data into Food, FoodDate tables...")

    insert_stmt = """INSERT INTO Food(foodName, foodCalorie, foodFat, foodCholesterol, foodSodium, foodCarb, foodSugar, foodProtein, foodServingTime, foodLocation)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

    insert_food_date = """INSERT INTO FoodDate(foodDateDate, foodID) VALUES (%s, %s)"""

    data_in_food_already = """SELECT * FROM Food WHERE foodName = %s AND foodLocation = %s"""

    date_in_fooddate_already = """SELECT * FROM FoodDate WHERE foodDateDate = %s AND foodId = %s"""

    for food in fooditerator:
        nutrients = food.nutrients
        food_data = (food.food_name, nutrients.calories, nutrients.fat, nutrients.cholesterol, nutrients.sodium, 
                     nutrients.carbohydrates, nutrients.sugar, nutrients.protein, food.serving_time, food.location)
        data_in_food_already_query = (food.food_name, food.location)

        try:
            # runs select query to get already present items of current food
            cursor.execute(data_in_food_already, data_in_food_already_query)

            '''
            if the (food, foodlocation) is not in the Food table, insert the food data
            into the food table and insert the associated food date.
            
            otherwise, insert the existing food item's current food date into the fooddate table.
            '''
            if len(cursor.fetchall()) == 0:
                cursor.execute(insert_stmt, food_data)
                cursor.execute(insert_food_date, (food.date, cursor.lastrowid))
            else:
                cursor.execute(data_in_food_already, data_in_food_already_query)
                existing_id = cursor.fetchone()[0]

                cursor.execute(date_in_fooddate_already, (food.date, existing_id))
                
                if len(cursor.fetchall()) == 0:
                    cursor.execute(insert_food_date, (food.date, existing_id))

            db_connection.commit()
        except Error as e:
                print(food_data) 
                print(e)
                db_connection.rollback()

    print("Successfully inserted food and date data into the vtmealplandb database!")

if __name__ == "__main__":
    password = "password"

    db_connection = mysql.connector.connect(
        host="127.0.0.1",
        user="root",
        password=password,
        database="vtmealplandb",
        connection_timeout=10
    )

    if db_connection.is_connected():
        db_info = db_connection.get_server_info()
        print("Connected to Database! Version: ", db_info)

    cursor = db_connection.cursor()
    fooditerator = scraper.FoodScraperIterator()
    
    insert_into_food_table(db_connection, cursor, fooditerator)

    delete_data(db_connection, cursor)