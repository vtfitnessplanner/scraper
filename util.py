import logging
import grequests

LOGGER = logging.getLogger()


def convert_string_to_grams(weight_string: str) -> float:
    unit_conversion = {
        'g': 1,
        'kg': 1000,
        'mg': 0.001,
    }

    # Find the index where the numeric value ends and the unit begins
    index = next(i for i, char in enumerate(weight_string) if char.isalpha())

    # Extract numeric value and unit
    numeric_value = weight_string[:index]
    unit = weight_string[index:]

    # Convert numeric value to float
    numeric_value = float(numeric_value)

    # Convert unit to lowercase for case-insensitive matching
    unit = unit.lower()

    # Check if the unit is valid
    if unit not in unit_conversion:
        raise ValueError("Invalid unit")

    # Multiply numeric value by conversion factor to get weight in grams
    weight_in_grams = numeric_value * unit_conversion[unit]

    return weight_in_grams


def grequest_exception_handler(request: grequests.request,
                               exception: Exception):
    LOGGER.exception(
        f"Exception occurred when trying to request: {request.url}."
        f"The following exception occurred:\n {exception}"
    )
