# Scraper

This is the repository that is responsible for the scrapper part of our project.

## Containerize the Scrapper Insert
Example:
```
docker build -t dwhassl/gobbleup-scraper-insert:prod1 .
docker push dwhassl/gobbleup-scraper-insert:prod1

kubectl apply -f ./kubernetes/scraper-insert-pod.yaml
```